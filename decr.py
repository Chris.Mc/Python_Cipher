#!/usr/bin/python3
# Vigenere Decryption
from a2num import a2num
from num2a import num2a

def decr(text):
    text = a2num(text)
    key = input("Enter the encryption key: \n")
    key = a2num(key)
    phrase_length = len(text)
    key_length = len(key)
    key_place = 0

    for z in range(0,phrase_length):
        if (type(text[z])==int):
            text[z] = text[z] - key[key_place]
            key_place = key_place+1
            key_place = key_place % key_length
            text[z] = text[z] % 26
    text = num2a(text)
    print("The decrypted text is: ")
    print(text)
    return text
