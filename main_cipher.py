#!/usr/bin/python3
from a2num import a2num
from num2a import num2a
from encr import encr
from decr import decr
from file_w import file_w
from file_r import file_r
from steg import steg


query = input("Enter 1 to for Steagnography\nEnter 2 to Encrypt, 3 to Decrypt:\n")
if (query =='1'):
	steg()
elif (query == '2'):
	query = input("Load text from a file? Y/N: ")
	if (query=='y'):
		text2encrypt = file_r()
	else:
		text2encrypt = input("Enter the text to encrypt:\n")
	encrypted_text=encr(text2encrypt)
	query = input("Write results to a file? Y/N: ")
	query = query.lower()
	if (query == 'y'):
	    file_w(encrypted_text)
elif(query == '3'):
	query = input("Load text from a file? Y/N:")
	if (query=='y'):
		text2decrypt = file_r()
	else:
		text2decrypt = input("Enter the text to decrypt:\n")
	decrypted_text=decr(text2decrypt)
	query = input("Write results to a file? Y/N: ")
	query = query.lower()
	if (query == 'y'):
	    file_w(decrypted_text)
else:
	print("Option 4")
