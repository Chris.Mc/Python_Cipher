#!/usr/bin/python3
#Convert input numeric array to text
def num2a(input_num):
	alpha = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
	text =[]
	string=""
	length = len(input_num)
	for x in range(0,length):
		number = input_num[x]
		if (type(number) == int):
			for y in range(0,26):
				if (number == y):
					number = alpha[y]
					text.append(number)
		else:
			text.append(number)
	string=''.join(text)
	return string
