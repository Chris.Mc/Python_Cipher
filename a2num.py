#!/usr/bin/python3

#Convert input text to numeric array
def a2num(input_text):
	input_text = input_text.lower()
	alpha = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
	numeric =[]
	length = len(input_text)
	for x in range(0,length):
		letter = input_text[x]
		if (letter.islower()):
			for y in range(0,26):
				if (letter == alpha[y]):
					letter = y
					numeric.append(letter)
		else:
			numeric.append(letter)
	return numeric 